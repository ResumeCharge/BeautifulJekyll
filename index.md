---
layout: home
title: Linda Fike
subtitle: Freelance Marketing Professional
---
<h3>Welcome to my website! I'm Linda, a freelance marketing professional. I've worked with teams of 5 people and 500 people to deliver professional marketing campaigns that inspire and motivate customers.
I've crafted marketing campaigns that were seen across the globe and reached customers from many backgrounds, demographics, and cultures.
No project is too small or too big to handle, take a look around my website to see my skills and projects, and reach out if you'd like to learn more.
</h3>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<a href="">Resume</a>