---
layout: page
title: Marketing Lead
permalink: /workexperience/marketing-lead
---
### Date : January 2014 - May 2018
### Location : Toronto, ON
### Title : Google Devices Marketing Lead

#### Projects:
* Managed teams of 20+ individuals and directed marketing campaigns for Google devices.
